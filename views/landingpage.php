<!DOCTYPE html>
<html>
<head>
	<title>Landing Page</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/cyborg/bootstrap.css">
</head>
<body>
	<div class="d-flex flex-column justify-content-center align-items-center vh-100">
	<?php
	session_start();

	?>
	<h1 class="text-info">Hello 
		<?php echo $_SESSION['fullName']?>!</h1>
	<h1 class="text-info">Your Birthday is 
		<?php echo $_SESSION['birthMonth']. " " .$_SESSION['birthDate']?></h1>
	<h1 class="text-info">You Are <?php echo $_SESSION['zodiac']?>!</h1>	
	</div>	

</body>
</html>